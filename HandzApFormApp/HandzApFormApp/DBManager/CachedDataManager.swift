//
//  CachedDataManager.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 24/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import Foundation

class CachedDataManager {
    private var listingFormData = [FormListModelData]()
    static let sharedCached = CachedDataManager()

    private init() {}
    
    func addFormData(newForm: FormListModelData) {
        listingFormData.append(newForm)
    }
    
    func deleteFormData(deletingFormData: FormListModelData) {
        listingFormData.removeAll(where: {$0 == deletingFormData})
    }
    func getAllListedData() -> [FormListModelData] {
        return listingFormData
    }
}

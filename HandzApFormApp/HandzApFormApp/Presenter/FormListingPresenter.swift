//
//  FormListingPresenter.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 24/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import Foundation
import UIKit

protocol FormListingPresenterProtocol: class {
    func allListedFormData(allListedForms: [FormListModelData]?)
}

class FormListingPresenter {
    
    weak private var formListingViewDelegate: FormListingPresenterProtocol?
    let formListingServices: FormListingServices!
    
    init(formListingServices: FormListingServices) {
        self.formListingServices = formListingServices
    }
    
    func setViewDelegate(formListingViewDelegate: FormListingPresenterProtocol)  {
        self.formListingViewDelegate = formListingViewDelegate
    }
    
    @discardableResult
    func getAllListedForm()-> [FormListModelData] {
        let allForms: [FormListModelData] =  formListingServices.getAllListedForm()
         formListingViewDelegate?.allListedFormData(allListedForms: allForms)
        return allForms
    }
    
    func deleteFormListData(deletingObject:FormListModelData) {
        formListingServices?.deleteFormListData(deletingObject: deletingObject)
        getAllListedForm()
    }
}

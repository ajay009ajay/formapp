//
//  NewFormPresenter.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 24/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import Foundation
import UIKit

protocol NewFormPresenterProtocol: class {
    func ratePickerValue(ratePickerValue: [String]?)
    func paymentPickerValue(paymentPickerValue: [String]?)
    func startDatePickerValue(startDatePickerValue: [String]?)
    func jobTermPickerValue(jobTermPickerValue: [String]?)
}
class NewFormPresenter {
    
    weak private var newFormViewDelegate: NewFormPresenterProtocol?
    let newFormListServices: NewFormListServices?
    private var attachments = [UIImage]()
    
    init(newFormListServices: NewFormListServices) {
        self.newFormListServices = newFormListServices
    }
    
    func setViewDelegate(newFormViewDelegate: NewFormPresenterProtocol) {
        self.newFormViewDelegate = newFormViewDelegate
    }
    
    @discardableResult
    func getRatePickerValue()-> [String] {
        
        if let newFormViewDelegate = newFormViewDelegate, let newFormListServices = newFormListServices  {
            let pickerValues = newFormListServices.getRatePickerValue()
            newFormViewDelegate.ratePickerValue(ratePickerValue: pickerValues)
            return pickerValues
        }
        return []
    }
    
    @discardableResult
    func getPaymentPickerValue()-> [String] {
        if let newFormViewDelegate = newFormViewDelegate, let newFormListServices = newFormListServices  {
            let pickerValues = newFormListServices.getPaymentPickerValue()
            newFormViewDelegate.paymentPickerValue(paymentPickerValue: newFormListServices.getPaymentPickerValue())
             return pickerValues
        }
        return []
    }
    
    @discardableResult
    func getStartDatePickerValue()-> [String]  {
        if let newFormViewDelegate = newFormViewDelegate, let newFormListServices = newFormListServices  {
            let pickerValues = newFormListServices.getStartDatePickerValue()
            newFormViewDelegate.startDatePickerValue(startDatePickerValue: pickerValues)
            return pickerValues
        }
        return []
    }
    
    @discardableResult
    func getJobTermPickerValue()-> [String]  {
        if let newFormViewDelegate = newFormViewDelegate, let newFormListServices = newFormListServices  {
            let pickerValues = newFormListServices.getJobTermPickerValue()
            newFormViewDelegate.jobTermPickerValue(jobTermPickerValue: pickerValues)
            return pickerValues
        }
        return []
    }
    
    func saveAttachemntImageInMemory(image: UIImage) {
        attachments.append(image)
    }
    
    func getAllAttachments()-> [UIImage]?  {
        return attachments
    }
    
    func getAttachmentImageForCell(row:Int) -> UIImage? {
        
        if row > attachments.count || row < 0{
            return nil
        }
        return attachments[row]
    }
    
    func addFormData(newFormData: FormListModelData) {
        newFormListServices?.addFormData(newFormData: newFormData)
    }
}

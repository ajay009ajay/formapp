//
//  RootViewController.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 23/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        customizeNavigationBar()
    }
    
    private func customizeNavigationBar()  {
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 44.0/255.0, green: 78.0/255.0, blue: 104.0/255.0, alpha: 1.0)
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  NewFormViewController.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 23/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import UIKit

class NewFormViewController: RootViewController, UICollectionViewDelegate, UICollectionViewDataSource, NewFormPresenterProtocol, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AddAttachmentProtocol {
    
    @IBOutlet weak var attachmentView: AttachmentView!
    
    @IBOutlet weak var formTitle: CustomTextField!
    @IBOutlet weak var formDescription: CustomTextField!
    @IBOutlet weak var formBudget: CustomTextField!
    @IBOutlet weak var formRate: CustomTextField!
    @IBOutlet weak var formPaymentMethod: CustomTextField!
    @IBOutlet weak var formStartDate: CustomTextField!
    @IBOutlet weak var formJobTerm: CustomTextField!

    private var allFormFields = [CustomTextField]()
    var newFormListPresenter = NewFormPresenter(newFormListServices: NewFormListServices())
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        initPresenterAndDelegateMethod()
        attachmentView.delegate = self
        
        customInitForAttachmentView()
        formTitleConfigInit()
        formDescriptionConfigInit()
        formBudgetConfigInit()
        
        allFormFields = [formTitle, formDescription, formBudget,formRate ,formPaymentMethod, formStartDate,formJobTerm]
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func initPresenterAndDelegateMethod() {
        
        newFormListPresenter.setViewDelegate(newFormViewDelegate: self)
        newFormListPresenter.getRatePickerValue()
        newFormListPresenter.getPaymentPickerValue()
        newFormListPresenter.getStartDatePickerValue()
        newFormListPresenter.getJobTermPickerValue()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "New Form"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send", style: .plain, target: self, action: #selector(sendButtonTapped))

        navigationItem.rightBarButtonItem?.isEnabled = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldEndEditing), name: NSNotification.Name(rawValue: "TextFieldSelected"), object: nil)

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func textFieldEndEditing(textView: CustomTextField)  {
        print("End Editing \(textView)")
        
        for eachFormField in allFormFields {
            if eachFormField.isPlaceholderPresent, eachFormField.isRequiredFormField  {
                eachFormField.highlightError()
            }
        }
        enableRightNavigationSendButton(enable: !isErrorInFormValidation())
    }
    
  
    
    //MARK: Custom Init
    
    private func customInitForAttachmentView()  {
        attachmentView.attachmentCollectionView.register(AttachmentCellView.self, forCellWithReuseIdentifier: "AttachmentCellView")

        attachmentView.attachmentCollectionView.delegate = self
        attachmentView.attachmentCollectionView.dataSource = self
    }
    
    private func formTitleConfigInit() {
        formTitle.refreshTextFieldView(placeholderText: "Form Title", maxCharacterLimit: 50, isRequiredType: true,textFieldType: .normalTextFieldType,pickerValues: nil,keyboardType: .default)
    }
    
    private func formDescriptionConfigInit() {
        formDescription.refreshTextFieldView(placeholderText: "Form Description", maxCharacterLimit: 330, isRequiredType: false, textFieldType: .descriptionType, pickerValues: nil,keyboardType: .default)
    }

    private func formBudgetConfigInit() {
        formBudget.refreshTextFieldView(placeholderText: "Budget", maxCharacterLimit: Int.max, isRequiredType: true, textFieldType: .descriptionType, pickerValues: nil,keyboardType: .numberPad)
    }
    
    private func formRateConfigInit(gridData:[String]?) {
        
        if let pickerValues = gridData {
            formRate.refreshTextFieldView(placeholderText: "Rate", maxCharacterLimit: Int.max, isRequiredType: false, textFieldType: .pickerType, pickerValues: pickerValues,keyboardType: .default)
        }
    }

    private func formPaymentMethodConfigInit(gridData:[String]?) {
        
        if let pickerValues = gridData {
            formPaymentMethod.refreshTextFieldView(placeholderText: "Payment Method", maxCharacterLimit: Int.max, isRequiredType: false, textFieldType: .pickerType, pickerValues: pickerValues,keyboardType: .default)
        }
    }
    
    private func formStartDateConfigInit(gridData:[String]?) {
        
        if let pickerValues = gridData {
            formStartDate.refreshTextFieldView(placeholderText: "Start Date", maxCharacterLimit: Int.max, isRequiredType: true, textFieldType: .pickerType, pickerValues: pickerValues,keyboardType: .default)
        }
    }
    
    private func formJobTermConfigInit(gridData:[String]?) {
        
        if let pickerValues = gridData {
            formJobTerm.refreshTextFieldView(placeholderText: "Job Term", maxCharacterLimit: Int.max, isRequiredType: false, textFieldType: .pickerType, pickerValues: pickerValues,keyboardType: .default)
        }
    }

    //MARK: Selector Action
    
    
    @objc func sendButtonTapped() {
 
        if !isErrorInFormValidation() {
            var caption = ""
            var rateVal = ""
            var stDate = ""

            if let title = formTitle.text {
                caption = title
            }
            if let rate = formRate.text {
                rateVal = rate
            }
            if let startDate = formStartDate.text {
                stDate = startDate
            }

            let desc = formDescription.text
            let budget = formBudget.text
            let paymentMethod = formPaymentMethod.text
            let jobterm = formJobTerm.text
            
            let newFormData = FormListModelData(formTitle: caption, description: desc, budget: budget, rate: rateVal, paymentMethod: paymentMethod, startDate: stDate, jobTerm: jobterm,attachments:newFormListPresenter.getAllAttachments())
            
            newFormListPresenter.addFormData(newFormData: newFormData)
            navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        for eachFormField in allFormFields {
            if !eachFormField.isRequiredFormField  {
                eachFormField.hideKeyboard()
            }
        }
        enableRightNavigationSendButton(enable: !isErrorInFormValidation())
    }
    
    //MARK: Validate Error
    
    private func isErrorInFormValidation()-> Bool {
        var isErrorInForm = false
        for eachFormField in allFormFields {
            if eachFormField.isPlaceholderPresent, eachFormField.isRequiredFormField  {
                eachFormField.highlightError()
                isErrorInForm = true
            }
        }
        return isErrorInForm
        
    }
    func enableRightNavigationSendButton(enable: Bool) {
        navigationItem.rightBarButtonItem?.isEnabled = enable
    }
    
    //MARK: NewForm Protocol
    func ratePickerValue(ratePickerValue: [String]?) {
        formRateConfigInit(gridData: ratePickerValue)
    }
    
    func paymentPickerValue(paymentPickerValue: [String]?) {
        formPaymentMethodConfigInit(gridData: paymentPickerValue)
    }
    
    func startDatePickerValue(startDatePickerValue: [String]?) {
        formStartDateConfigInit(gridData: startDatePickerValue)
    }
    
    func jobTermPickerValue(jobTermPickerValue: [String]?) {
        formJobTermConfigInit(gridData: jobTermPickerValue)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK: Attachment galley delegate
    
    func addAttachmentBtnClicked()  {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .savedPhotosAlbum
            myPickerController.allowsEditing = false
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
 
        guard let image = info[.originalImage] as? UIImage else {
            print("Expected Image but not valid data: \(info)")
            return
        }
        newFormListPresenter.saveAttachemntImageInMemory(image: image)
        attachmentView.attachmentCollectionView.reloadData()
        
        print("UIIMage: \(image)")

        self.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
    
    // MARK: CollectionView Delegate
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let imageCount = newFormListPresenter.getAllAttachments()?.count, imageCount > 0 {
            return imageCount
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCellView", for: indexPath) as? AttachmentCellView
        cell?.attachmentCellImageView.image = newFormListPresenter.getAttachmentImageForCell(row: indexPath.row)
        return cell!
    }
}

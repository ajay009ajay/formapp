//
//  FormLitsingViewController.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 23/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import UIKit

class FormLitsingViewController: RootViewController, FormListingPresenterProtocol, FormDeleteProtocol  {
    
    @IBOutlet weak var containerScrollView: UIScrollView!
    
    var fomrListingPresenter = FormListingPresenter(formListingServices: FormListingServices())
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        fomrListingPresenter.setViewDelegate(formListingViewDelegate: self)
        
        containerScrollView.accessibilityIdentifier = "formListScrollViewAccessIdentifier"
        
    }
    
    func addFormsInContainer(allForms: [FormListModelData]?) {
       
        if containerScrollView.subviews.count > 0 {
            _ =  containerScrollView.subviews.map({$0.removeFromSuperview()})
        }
        
            var yPos = 0
            if let allFormArr = allForms {
                for eachListing in allFormArr {
                    let formListView = FormListingView(frame: CGRect(x: 0, y: yPos, width: Int(UIScreen.main.bounds.size.width), height: 195))
                    formListView.delegate = self
                    formListView.objectID = eachListing
                    containerScrollView.addSubview(formListView)

                    formListView.formTitle.text = eachListing.formTitle
                    formListView.startDate.text = eachListing.startDate
                    formListView.rateVal.setTitle(eachListing.rate, for: .normal)
                    formListView.jobTermVal.setTitle(eachListing.jobTerm, for: .normal)
                    yPos += 195
                    containerScrollView.contentSize = CGSize(width:Int(UIScreen.main.bounds.size.width),height:yPos)
                }
            }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Form Listing"
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "plus"), style: .plain, target: self, action: #selector(addTapped))
        fomrListingPresenter.getAllListedForm()
    }

    @objc func addTapped()  {
        print("Add tapped")
        self.performSegue(withIdentifier: "NewFormScreen", sender: nil)
    }
    
    func deleteForm(deletingObject: FormListModelData?) {
        let actionSheetController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        // Create and add the Cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            // Just dismiss the action sheet
            self.dismiss(animated: true, completion: nil)
        }
        actionSheetController.addAction(cancelAction)
        let image = UIImage(named: "Trash_icon.png")
        let deleteImgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 48, height: 48))
        deleteImgView.image = image
        let deleteAction = UIAlertAction(title: "Delete Form", style: .default) {[weak self] (action) in
            if  let deletingObject = deletingObject {
                self?.fomrListingPresenter.deleteFormListData(deletingObject: deletingObject)
            }
        }
        deleteAction.setValue(image, forKey: "image")
        deleteAction.setValue(UIColor.red, forKey: "titleTextColor")
        actionSheetController.addAction(deleteAction)
        self.navigationController?.present(actionSheetController, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK: FormListing Presenter protocol
    
    func allListedFormData(allListedForms: [FormListModelData]?) {
        addFormsInContainer(allForms: allListedForms)
    }
    
}

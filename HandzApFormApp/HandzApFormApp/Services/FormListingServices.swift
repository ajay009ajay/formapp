//
//  FormListingServices.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 24/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import Foundation
import UIKit

class FormListingServices {
    let sharedCached = CachedDataManager.sharedCached
    
    func getAllListedForm() -> [FormListModelData] {
        return sharedCached.getAllListedData()
    }
    func deleteFormListData(deletingObject:FormListModelData) {
         sharedCached.deleteFormData(deletingFormData: deletingObject)
    }
}

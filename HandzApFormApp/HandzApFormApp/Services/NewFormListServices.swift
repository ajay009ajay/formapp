//
//  NewFormListServices.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 24/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    func allDates(till endDate: Date) -> [String] {
        var date = self
        var array: [String] = []
        while date <= endDate {
            
            let formatter = DateFormatter()
            // initially set the format based on your datepicker date / server String
            formatter.dateFormat = "yyyy-MM-dd"
            
            let myString = formatter.string(from: date) // string purpose I add here
            // convert your string to date
            let yourDate = formatter.date(from: myString)
            //then again set the date format whhich type of output you need
            formatter.dateFormat = "dd-MMM-yyyy"
            // again convert your date to string
            let myStringafd = formatter.string(from: yourDate!)
            
            print(myStringafd)
            
            array.append(myStringafd)
            date = Calendar.current.date(byAdding: .day, value: 1, to: date)!
        }
        return array
    }
}

struct NewFormListServices {
    let sharedCached = CachedDataManager.sharedCached

    func getRatePickerValue() -> [String] {
        return ["No Preferences", "Fixed Rate","Hourly Rate"]
    }
    
    func getPaymentPickerValue() -> [String] {
        return ["No Preferences", "Cash", "E-Payment"]
    }

    func getStartDatePickerValue() -> [String]  {
        
        if let date = Calendar.current.date(byAdding: .day, value: 20, to: Date()) {
            print(Date().allDates(till: date))
             return Date().allDates(till: date)
        }
       return [""]
    }
    
    func getJobTermPickerValue() -> [String]  {
        return ["No Preferences", "Recurring Job", "Same Day Job", "MultiDays Job"]
    }
    
    func addFormData(newFormData: FormListModelData) {
        sharedCached.addFormData(newForm: newFormData)
    }
}

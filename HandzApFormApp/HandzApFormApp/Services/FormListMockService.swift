//
//  FormListMockService.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 25/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import Foundation

import Foundation
import UIKit

class FormListMockService: FormListingServices {

    public func addMockData(formData: FormListModelData)  {
        sharedCached.addFormData(newForm: formData)
    }
}

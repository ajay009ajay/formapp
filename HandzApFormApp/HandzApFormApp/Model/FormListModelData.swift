//
//  FormListModelData.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 24/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import Foundation
import UIKit

class FormListModelData:BaseModel  {
    private  (set) var formTitle : String
    private  (set) var formDescription: String?
    private  (set) var budget: String?
    private  (set) var currencyType = "USD"
    private  (set) var rate: String
    private  (set) var paymentMethod: String?
    private  (set) var startDate: String
    private  (set) var jobTerm: String?
    private  (set) var attachments: [UIImage]?

     init(formTitle: String, description: String?,budget: String?, rate: String, paymentMethod: String?,startDate: String, jobTerm: String?, attachments: [UIImage]?) {
        self.formTitle = formTitle
        self.formDescription = description
        self.budget = budget
        self.rate = rate
        self.paymentMethod = paymentMethod
        self.startDate = startDate
        self.jobTerm = jobTerm
        self.attachments = attachments
    }
    
    
}

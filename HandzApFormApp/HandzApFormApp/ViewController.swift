//
//  ViewController.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 22/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import UIKit

 
class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    func cancelClicked() {
        print("Parent cancel")
    }
    
    func doneBtnClicked() {
        print("parent done clicked")
    }
    
   
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCellView", for: indexPath) as? AttachmentCellView
        return cell!
    }
    

    @IBOutlet weak var formTitle_1: CustomTextField!
    @IBOutlet weak var formTitle_2: CustomTextField!
    @IBOutlet weak var formTitle_3: CustomTextField!

    @IBOutlet weak var attachmentView: AttachmentView!
    @IBOutlet weak var customPickerView: CustomPickerView!
    
    let arrVal = ["dkdk","fhfghg","wgdgfdg","trgdgdg","vcbv"]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let attachmentCellViewNib = UINib(nibName: "AttachmentCellView", bundle: nil)
        attachmentCellViewNib.instantiate(withOwner: attachmentView, options: nil)
        attachmentView.attachmentCollectionView.register(attachmentCellViewNib, forCellWithReuseIdentifier: "AttachmentCellView")
        
        attachmentView.attachmentCollectionView.delegate = self
        attachmentView.attachmentCollectionView.dataSource = self
        
//        customPickerView.pickerDelegate = self
//        customPickerView.pickerSource = arrVal
//        customPickerView.pickerView.reloadAllComponents()
        
//        formTitle_1.maxCharacterLimit = 50
//        formTitle_1.updatePlaceholderText(placeholderText: "Form Title")
//        formTitle_1.isRequiredType = true
//        formTitle_1.refreshTextFieldView(placeholderText: "Form Title", maxCharacterLimit: 50, isRequiredType: true, pickerValues: nil)
    }
    
}

//
//  CustomTextField.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 22/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import UIKit


enum UITextFieldType {
    case pickerType
    case descriptionType
    case normalTextFieldType
}

class CustomTextField: UIView, UITextViewDelegate, CustomPickerProtocol {
   
    @IBOutlet private weak var textFieldTitle: UILabel!
    @IBOutlet private weak var bottomTitle: UILabel!
    @IBOutlet private var contentView: UIView!
    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var bottomBorderLine: UIView!
    @IBOutlet private weak var pickerTextBtn: UIButton!

    private let defaultColor = UIColor(red: 68.0/255.0, green: 92.0/255.0, blue: 149.0/255.0, alpha: 1.0)

    private var placeholderText = ""
    private var pickerValues: [String]? = [String]()
    private var maxCharacterLimit = Int.max
    private var isRequiredType = false
    private var textFieldType = UITextFieldType.normalTextFieldType
    private var prevRect = CGRect.zero

    private var customPickerView: CustomPickerView?
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var caption: String? {
        get { return textFieldTitle?.text}
        set { textFieldTitle?.text = newValue}
    }
    var textfieldAlertMessage: String? {
        get {   return bottomTitle?.text   }
        set {   bottomTitle?.text = newValue }
    }
    var text: String? {
        get {
            if textFieldType == .pickerType {
                return pickerTextBtn.titleLabel?.text
            } else {
                return textView.text
            }
        }
    }
    
    var isPlaceholderPresent: Bool {
        get {
            if textFieldType != .pickerType {
                return placeholderText.count > 0
            }
            return false
        }
    }
    
    var isRequiredFormField: Bool {
        get { return isRequiredType }
     }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSubviews()
        updateToDefaultTextColor()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }
    
    private func initSubviews() {
        // standard initialization logic
        let nib = UINib(nibName: "CustomTextField", bundle:Bundle(for: type(of: self)))
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }    
    
    //MARK: UItextView delegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeholderText {
            textView.text = ""
            textView.textColor = defaultColor
            textFieldTitle.text = placeholderText
            textFieldTitle.isHidden = false
            bottomTitle.isHidden = false
            placeholderText = ""
            resetHighlightError()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TextFieldSelected"), object: self)
        }
        
        if maxCharacterLimit < Int.max {
            updateMaxCharTitleAtBottom(characterCount: maxCharacterLimit)
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.canResignFirstResponder {
            textView.resignFirstResponder()
        }
        if let titleCount = textFieldTitle.text?.count, placeholderText.count <= 0 && titleCount > 0 && textView.text.count <= 0{
            updatePlaceholderText(placeholderText: textFieldTitle.text)
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TextFieldSelected"), object: self)
    }

    func textViewDidChange(_ textView: UITextView) {
        let pos = textView.endOfDocument
        let currentRect = textView.caretRect(for: pos)
        if currentRect.origin.y > prevRect.origin.y && textView.text.count > 0 && textFieldType == .descriptionType{
            print("New line")
        }
        prevRect = currentRect
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let textStr = textView.text + text
        
        if maxCharacterLimit < Int.max {
            let characterCount = (text.count <= 0) ? (maxCharacterLimit - textStr.count - 1) : (maxCharacterLimit - textStr.count)
            updateMaxCharTitleAtBottom(characterCount: characterCount)
            return characterCount > 0
        }
            
        return true
    }
    
    
    // MARK: Helper
    
    private func updatePlaceholderText(placeholderText: String?) {
        
        guard let placeholderText = placeholderText else {
            return
        }
        textView.text = placeholderText
        textView.textColor = UIColor.lightGray
        self.placeholderText = placeholderText
        textFieldTitle.isHidden = true
    }
    
    func updateMaxCharTitleAtBottom(characterCount:Int) {
        bottomTitle.text = "\(characterCount) characters left"
    }
    
    func updateToDefaultTextColor() {
        textFieldTitle.textColor = defaultColor
        bottomTitle.textColor = UIColor.lightGray
    }
    
    func resetHighlightError() {
        bottomBorderLine.backgroundColor = UIColor.lightGray
        bottomTitle.textColor = UIColor.lightGray
    }
    
    func highlightError() {
        let tmpPlacseholderText = (textFieldType == .pickerType) ? pickerTextBtn.titleLabel?.text : textView.text
        updatePlaceholderText(placeholderText: tmpPlacseholderText)
        textFieldTitle.text = ""
        textFieldTitle.isHidden = true
        textView.text = placeholderText
        bottomBorderLine.backgroundColor = UIColor.red
        bottomTitle.textColor = UIColor.red
        bottomTitle.text = "Required"
    }
    
    func setupPickerUIOnTextFieldUI()  {
        textView.isHidden = true
        pickerTextBtn.isHidden = false
        pickerTextBtn.setTitle(placeholderText, for: .normal)
        pickerTextBtn.titleLabel?.textColor = UIColor.lightGray
        pickerTextBtn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        pickerTextBtn.contentVerticalAlignment = UIControl.ContentVerticalAlignment.top
        pickerTextBtn.setTitleColor(UIColor.lightGray, for: .normal)
        self.textFieldType = .pickerType

    }
    
    func refreshTextFieldView(placeholderText:String, maxCharacterLimit:Int, isRequiredType: Bool, textFieldType: UITextFieldType, pickerValues:[String]?, keyboardType: UIKeyboardType)  {
        bottomTitle.isHidden = !isRequiredType
        self.maxCharacterLimit = maxCharacterLimit
        self.isRequiredType = isRequiredType
        self.pickerValues = pickerValues
        self.placeholderText = placeholderText
        
        if textFieldType == .pickerType {
            setupPickerUIOnTextFieldUI()
        } else {
            updatePlaceholderText(placeholderText: placeholderText)
            self.textFieldType = textFieldType
            self.textView.keyboardType = keyboardType

        }
        
    }
    
    @IBAction func pickerBtnClicked(_ sender: Any) {
        
        if let pickerValue = pickerValues,
           let picker = CustomPickerView(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height - 250, width: UIScreen.main.bounds.size.width, height: 250), pickerValues: pickerValue, delegate: self)
            {
                customPickerView = picker
                self.window?.addSubview(picker)
            }
    }
    
    func hideKeyboard()  {
        if textView.canResignFirstResponder {
            textView.resignFirstResponder()
        }
    }
    // MARK: Custom picker delegate
    private func removePickerFromView() {
        if customPickerView != nil {
            customPickerView?.removeFromSuperview()
            customPickerView = nil
        }
    }
    
    func cancelClicked() {
        removePickerFromView()
    }
    
    func doneBtnClicked(selectedValue: String?) {
        if let selectedValueFromPicker = selectedValue {
            pickerTextBtn.setTitle(selectedValueFromPicker, for: .normal)
            pickerTextBtn.setTitleColor(defaultColor, for: .normal)
            resetHighlightError()
        }
        removePickerFromView()
    }
    
}

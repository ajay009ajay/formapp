//
//  AttachmentCellView.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 23/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import UIKit

class AttachmentCellView: UICollectionViewCell {
      var attachmentCellImageView: UIImageView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
//
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        attachmentCellImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 90, height: 90))
        self.addSubview(attachmentCellImageView)
    }
}

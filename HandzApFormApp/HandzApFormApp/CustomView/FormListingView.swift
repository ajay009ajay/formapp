//
//  FormListingView.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 24/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import UIKit
import QuartzCore

protocol FormDeleteProtocol: class {
    func deleteForm(deletingObject: FormListModelData?)
}

class FormListingView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var inviteBtn: UIButton!
    @IBOutlet weak var inboxBtn: UIButton!
    
    @IBOutlet weak var formTitle: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var rateVal: UIButton!
    @IBOutlet weak var jobTermVal: UIButton!
    @IBOutlet weak var deleteActioButton: UIView!
    
    weak var delegate: FormDeleteProtocol?
    var objectID: FormListModelData?
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }
    
    private func initSubviews() {
        // standard initialization logic
        let nib = UINib(nibName: "FormListingView", bundle:Bundle(for: type(of: self)))
        nib.instantiate(withOwner: self, options: nil)
        
        contentView.frame = bounds
        addSubview(contentView)
        // custom initialization logic
        
        inboxBtn.layer.borderWidth = 1.0
        inboxBtn.layer.borderColor = UIColor(red: 44.0/255.0, green: 78.0/255.0, blue: 104.0/255.0, alpha: 1.0).cgColor
        
        self.accessibilityIdentifier = "ScrollViewFormListSubView"
        self.deleteActioButton.accessibilityIdentifier = "ScrollViewFormListSubViewDeleteActionButton"
    }
    
    @IBAction func actionDeleteCalled(_ sender: Any) {
        
        if let formDelegate = delegate {
            formDelegate.deleteForm(deletingObject: objectID)
        }
    }
    
}

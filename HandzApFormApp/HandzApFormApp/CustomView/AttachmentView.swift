//
//  AttachmentView.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 23/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import UIKit

protocol AddAttachmentProtocol:class {
    func addAttachmentBtnClicked()
}

class AttachmentView: UIView {
    
    @IBOutlet weak var attachmentTitleLabel: UILabel!
    @IBOutlet weak var attachmentPlusBtn: UIButton!
    @IBOutlet weak var attachmentCollectionView: UICollectionView!
    @IBOutlet var contentView: UIView!
    
    weak var delegate: AddAttachmentProtocol?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
   
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }
    
    private func initSubviews() {
        // standard initialization logic
        let attachmentNib = UINib(nibName: "AttachmentView", bundle:Bundle(for: type(of: self)))
        attachmentNib.instantiate(withOwner: self, options: nil)
 
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    @IBAction func addAttachment(_ sender: Any) {
        if let lclDelegate = delegate {
            lclDelegate.addAttachmentBtnClicked()
        }
    }
    
}

//
//  CustomPickerView.swift
//  HandzApFormApp
//
//  Created by Apple Imac on 23/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import UIKit

protocol CustomPickerProtocol: class {
    func cancelClicked()
    func doneBtnClicked(selectedValue: String?)
}

class CustomPickerView: UIView,UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBAction func cancelClicked() {
        print("Cancel clicked")
        pickerDelegate?.cancelClicked()
    }
  
    @IBAction func doneBtnClicked() {
        print("Done clicked")
        pickerDelegate?.doneBtnClicked(selectedValue: seletedValue)
    }
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    
    private var pickerSource = [String]()
    private var pickerDelegate: CustomPickerProtocol?
    private var seletedValue : String?

    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSubviews()
    }
    
    init?(frame: CGRect, pickerValues: [String], delegate:CustomPickerProtocol?) {
        super.init(frame: frame)
        self.pickerSource = pickerValues
        self.pickerDelegate = delegate
        initSubviews()
    }
    
    private func initSubviews() {
        // standard initialization logic
        let attachmentNib = UINib(nibName: "CustomPickerView", bundle:Bundle(for: type(of: self)))
        attachmentNib.instantiate(withOwner: self, options: nil)
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        contentView.frame = bounds
        addSubview(contentView)
        // custom initialization logic
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerSource.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerSource[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        seletedValue = pickerSource[row]
        print("\(pickerSource[row])")
    }
}

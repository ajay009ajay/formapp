//
//  HandzApNewFormUnitTests.swift
//  HandzApFormAppTests
//
//  Created by Apple Imac on 25/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import XCTest
@testable import HandzApFormApp

class HandzApNewFormUnitTests: XCTestCase {

    var formVC: NewFormViewController!
    var service = NewFormListServices()
    var presenter: NewFormPresenter!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle:Bundle(for: type(of: self)))
        let viewController: NewFormViewController = storyboard.instantiateViewController(withIdentifier: "NewFormVC") as! NewFormViewController
        let navigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        let window = UIWindow()
        window.rootViewController = navigationController
        navigationController.pushViewController(viewController, animated: false)
        window.makeKeyAndVisible()
        self.formVC = viewController
        self.formVC.loadView()
        viewController.view.layoutIfNeeded()
        
        presenter = NewFormPresenter(newFormListServices: service)
        presenter.setViewDelegate(newFormViewDelegate: viewController)
        self.formVC.loadView()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testPickerValues() {
        
        _ = formVC.view
        
        XCTAssertTrue(presenter.getRatePickerValue().count > 0, "Rate picker should present")
        XCTAssertTrue(presenter.getPaymentPickerValue().count > 0, "Payment picker should present")
        XCTAssertTrue(presenter.getStartDatePickerValue().count > 0, "Start date picker should present")
        XCTAssertTrue(presenter.getJobTermPickerValue().count > 0, "Job term picker should present")
    }

}

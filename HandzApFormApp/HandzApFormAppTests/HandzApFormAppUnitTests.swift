//
//  HandzApFormAppUnitTests.swift
//  HandzApFormAppTests
//
//  Created by Apple Imac on 25/01/20.
//  Copyright © 2020 Self. All rights reserved.
//

import XCTest
@testable import HandzApFormApp

class HandzApFormAppUnitTests: XCTestCase {

    var formVC: FormLitsingViewController!
    var mockService = FormListMockService()
    var formListPresenter: FormListingPresenter!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle:Bundle(for: type(of: self)))
        let viewController: FormLitsingViewController = storyboard.instantiateViewController(withIdentifier: "FormVC") as! FormLitsingViewController
        let navigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        let window = UIWindow()
        window.rootViewController = navigationController
        navigationController.pushViewController(viewController, animated: false)
        window.makeKeyAndVisible()
        self.formVC = viewController
        self.formVC.loadView()
        viewController.view.layoutIfNeeded()
        
        formListPresenter = FormListingPresenter(formListingServices: mockService)
        formListPresenter.setViewDelegate(formListingViewDelegate: viewController)
        self.formVC.loadView()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        
                RunLoop.main.run(until: NSDate() as Date)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testScrollViewExist() {
        let form_1 = FormListModelData(formTitle: "Test Form", description: "Test descriptoin", budget: "50", rate: "Rate", paymentMethod: "Cash", startDate: "Jan 25, 2020", jobTerm: "Daily", attachments: nil)
        mockService.addMockData(formData: form_1)
        formListPresenter.getAllListedForm()
        _ = formVC.view
        
        XCTAssertTrue(formVC.containerScrollView != nil, "Scrollview should present")
        XCTAssertTrue(formVC.containerScrollView.subviews.count > 0,"one data present")
        
    }

}
